export class User {
    id: string;
    email: string;
    fullName: string;
    isAdmin: boolean;
    status: boolean;
}
