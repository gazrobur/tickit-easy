import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  email: string;
  name: string;
  password: string;
  formGroup: FormGroup;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [Validators.required]);
  nameFormControl = new FormControl('', [Validators.required]);

  constructor(
    private router: Router,
    private authService: AuthService,
    ) {
      this.formGroup = new FormGroup({
        nameFormControl: this.nameFormControl,
        emailFormControl: this.emailFormControl,
        passwordFormControl: this.passwordFormControl
      });
      this.nameFormControl.valueChanges.subscribe(n => {
        this.name = n;
      }),
      this.emailFormControl.valueChanges.subscribe(e => {
        this.email = e;
      }),
      this.passwordFormControl.valueChanges.subscribe(p => {
        this.password = p;
      });
  }

  ngOnInit() { }

  async register() {
    const data = { email: this.email, password: this.password };
    const l = await this.authService.register(data);
    if (l.body) {
      this.router.navigate(['login']);
    }
  }

}
