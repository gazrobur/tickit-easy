import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;
  formGroup: FormGroup;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [Validators.required]);

  constructor(
    private router: Router,
    private authService: AuthService,
    ) {
      this.formGroup = new FormGroup({
        emailFormControl: this.emailFormControl,
        passwordFormControl: this.passwordFormControl
      });
      this.emailFormControl.valueChanges.subscribe(e => {
        this.email = e;
      }),
      this.passwordFormControl.valueChanges.subscribe(p => {
        this.password = p;
      });
  }

  ngOnInit() { }

  async login() {
    const data = { email: this.email, password: this.password };
    const l = await this.authService.login(data);
    if (l.body) {
      this.router.navigate(['purchase']);
    }
  }

}
