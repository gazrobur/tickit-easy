import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getUser(id: string): Promise<User> {
    return this.http.get<User>(`${this.baseUrl}/user/${id}`, {
      withCredentials: true
    }).toPromise();
  }

  getUsers() {
    return this.http.get(`${this.baseUrl}/user/all`, {
      withCredentials: true
    }).toPromise();
  }
}
