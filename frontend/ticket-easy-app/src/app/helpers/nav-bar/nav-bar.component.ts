import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  isLoggedIn: boolean;
  currentUser: User;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    ) { }

  async ngOnInit() {
    await this.isAuthenticated();
    await this.getCurrentUser();
   }

  async isAuthenticated() {
    const r = await this.authService.isLoggedIn();
    return r.body ? this.isLoggedIn = true : this.isLoggedIn = false;
  }

  async logout() {
    await this.authService.logout();
    this.router.navigate(['']);
  }

  async login(email: string, password: string) {
    const data = { email, password };
    await this.authService.login(data);
  }

  async getCurrentUser() {
    const user = await (await this.authService.isLoggedIn()).body as User;
    this.currentUser = await this.userService.getUser(user.id);
    console.log(this.currentUser);
  }
}
