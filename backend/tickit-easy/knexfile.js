require('dotenv').config();

module.exports = {
  development: {
    client: "pg",
    connection: "postgres://udjeezvkmzzvyz:35677c569e429c75aa14eb04ba28d68723352a2b512abf188688c8fddc853a0c@ec2-54-210-128-153.compute-1.amazonaws.com:5432/d48jvnjfct6euq",
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: "../nodebus/db/migrations",
      tableName: "knex_migrations"
    },
    seeds: {
      directory: "../nodebus/db/seeds"
    }
  },
  production: {
    client: 'pg',
    connection: "postgres://udjeezvkmzzvyz:35677c569e429c75aa14eb04ba28d68723352a2b512abf188688c8fddc853a0c@ec2-54-210-128-153.compute-1.amazonaws.com:5432/d48jvnjfct6euq",
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: "./db/migrations",
      tableName: "knex_migrations"
    },
    seeds: {
      directory: "./db/seeds"
    },
    ssl: true
  }
};
