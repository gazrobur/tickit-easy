
exports.up = function(knex, Promise) {
  return knex.schema
    .createTable('tickets', function (table) {
        table.increments('id').primary();
        table.integer('ticket_number', 11);
        table.integer('category_id').unsigned();
        table.integer('price', 11);
        table.text('comment');
        table.boolean('isSold');
        table.boolean('isAvailable');
        
        table.unique('id');
        table.foreign('category_id').references('id').inTable('ticket_category');
    })
    .then(() => {
        return knex.schema.table('transactions', table => {
          table.dropColumn('ticket_id');
        });
    })
    .then(() => {
      return knex.schema.table('transactions', table => {
        table.integer('ticket_id').unsigned();
        table.foreign('ticket_id').references('id').inTable('tickets');
      });
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('tickets')
    .then(() => {
        return knex.schema.table('transactions', table => {
          table.dropForeign('ticket_id').references('id').inTable('tickets');
        });
    })
};
