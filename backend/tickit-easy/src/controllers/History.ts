import * as web from 'advanced-controllers';
import { knex } from '../server/remoteDb';

@web.Controller('/history')
@web.Authorize()
export class HistoryController extends web.AdvancedController {
  @web.Get('/all')
  @web.Authorize()
  async getHistory() {
    try {
      const history = await knex('history').returning('*');
      return history;
    } catch (err) {
      throw new web.WebError('Table history does not exists!', 400);
    }
  }

  @web.Get('/:id')
  async getHistoryByUserId(@web.Param('id') userId: string) {
    try {
      const userHistory = await knex('history')
        .where({ user_id: userId })
        .returning('*');

      return userHistory;
    } catch (err) {
      throw new web.WebError('Table history does not exists!', 400);
    }
  }
}
