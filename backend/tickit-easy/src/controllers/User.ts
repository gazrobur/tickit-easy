import * as web from 'advanced-controllers';
import { IBaseUser, IUser, User } from '../model/User';
import { knex } from '../server/remoteDb';

@web.Controller('/user')
@web.Authorize()
export class UserController extends web.AdvancedController {
  @web.Get('/all')
  async getAllUsers(@web.Req() req: web.Request & { user }): Promise<User[]> {
    try {
      const users: IUser[] = await knex('users').returning('*');
      return users.map(u => new User(u));
    } catch (error) {
      console.log(error);
      throw new web.WebError('Table users does not exists!', 500);
    }
  }

  @web.Get('/:id')
  async getUserById(@web.Param('id') id: number): Promise<User> {
    try {
      const [res] = await knex('users').where('id', id);
      const user = new User(res);
      return user;
    } catch (error) {
      throw new web.WebError(`User with id ${id} does not exists!`, 500);
    }
  }

  @web.Get('/loggedIn')
  async getLoggedInUser(@web.Req() req: web.Request & { user }): Promise<User> {
    console.log(web.AdvancedController.getAllPermissions());
    console.log(req.user);
    return req.user;
  }

  @web.Put('/update/:id')
  @web.Permission('Admin')
  async updateUser(
    @web.Param('id') id: number,
    @web.Body() user: IBaseUser
  ): Promise<boolean> {
    const res = await knex('users')
      .where('id', id)
      .update(user);

    return !!res;
  }

  @web.Del('/delete/:id')
  @web.Permission('Admin')
  async deleteUser(@web.Param('id') id: number): Promise<boolean> {
    const res = await knex('users')
      .where('id', id)
      .del();
    return !!res;
  }
}
