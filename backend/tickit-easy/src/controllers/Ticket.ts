import * as web from 'advanced-controllers';
import { knex } from '../server/remoteDb';
import { ITicket } from '../model/Ticket';

export function isSuccess(ticket: number) {
  const success = [ticket]
  if (!success) {
    throw new web.WebError('Failed to update ticket!', 400);
  }
  return !!success;
}
@web.Controller('/ticket')
export class TicketController extends web.AdvancedController {

  @web.Get('/all')
  async getTickets(): Promise<ITicket[]> {
    try {
      const tickets: ITicket[] =  [{
      id: 0,
      ticketNumber: 0,
      categoryId: 0,
      price: 1290,
      comment: 'Student ticket',
      isSold: false,
      isAvailable: true,
      destination: 0,
      defaultPrice: 1290,
      name: 'Kistarcsa - Budapest'
    }];
      return tickets;
    } catch (error) {
      throw new web.WebError('Failed to get tickets!', 500);
    }
  }

  @web.Post('/create')
  @web.Permission('Admin')
  async createTicket(@web.Body() ticket: ITicket): Promise<boolean> {
    try {
        const [newTicket] = await knex('tickets').insert({ ...ticket });
        return !!newTicket;
    } catch (error) {
      throw new web.WebError('Failed to create ticket!');
    }
  }

  @web.Del('/delete/:id')
  @web.Permission('Admin')
  async deleteTicket(@web.Param('id') id: number): Promise<boolean> {
    const res = await knex('tickets')
      .where('id', id)
      .del();
    return !!res;
  }

  @web.Put('/enable/:id')
  async enableTicket(@web.Param('id') ticketId: number): Promise<boolean> {
    try {
      const ticket = await knex('tickets')
        .where('id', ticketId)
        .update('isAvailable', true);
      return isSuccess(ticket);
    } catch (error) {
      throw new web.WebError('Enabling ticket failed', 400);
    }
  }

  @web.Put('/disable/:id')
  async disableTicket(@web.Param('id') ticketId: number): Promise<boolean> {
    try {
      const ticket = await knex('tickets')
        .where('id', ticketId)
        .update('isAvailable', false);
      return isSuccess(ticket);
    } catch (error) {
      throw new web.WebError('Disabling ticket failed', 400);
    }
  }
}
