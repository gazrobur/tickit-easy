import * as web from 'advanced-controllers';
import { ILoginUser } from '../model/User';
import { knex } from '../server/remoteDb';
import { isPasswordCorrect, hashPassword } from '../util/password';
import { signToken, addAuthCookie, removeAuthCookie } from '../util/jwt';

@web.Controller('/auth')
export class LoginController extends web.AdvancedController {
  @web.Post('/login')
  async login(@web.Body() user: ILoginUser, @web.Res() res: web.Response) {
    try {
      const [userData] = await knex('users').where('email', user.email);
      if (!userData) {
        throw new web.WebError('User does not exists!', 400);
      } else {
        const validPassword = await isPasswordCorrect(
          user.password,
          userData.password
        );

        if (!validPassword) {
          throw new web.WebError('Helytelen jelszó!', 400);
        } else {
          // let permissions = await knex('user_permissions')
          //   .select('name')
          //   .join(
          //     'permissions',
          //     'user_permissions.permission_id',
          //     'permissions.id'
          //   )
          //   .where('user_id', userData.id);
          // permissions = permissions.map(p => p.name);

          // console.log('user permissions:', permissions);

          const token = await signToken({
            id: userData.id,
            email: userData.email,
            fullName: userData.fullName,
            // permissions
          });

          console.log('token:', token);

          const expiration = addAuthCookie(res, token);

          console.log('expiration:', expiration);

          res.send({ expiration });
        }
      }
    } catch (error) {
      throw new web.WebError(error.message, 500);
    }
  }

  @web.Post('/register')
  async createUser(@web.Body() user: ILoginUser): Promise<boolean> {
    try {
      const userExists = await knex('users').where('email', user.email);
      if (!userExists.length) {
        const password = await hashPassword(user.password);
        const [newUser] = await knex('users').insert({ ...user, password });
        return !!newUser;
      } else {
        throw new web.WebError('User with this email address already exists');
      }
    } catch (error) {
      throw new web.WebError((error as web.WebError).message);
    }
  }

  @web.Get('/logout')
  async logout(@web.Res() res: web.Response) {
    removeAuthCookie(res);
    res.sendStatus(200);
  }
}
