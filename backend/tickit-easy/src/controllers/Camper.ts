import * as web from 'advanced-controllers';
import { knex } from '../server/remoteDb';

@web.Controller('/camper')
@web.Authorize()
export class CamperController extends web.AdvancedController {
  @web.Get('/all')
  async getCamper() {
    try {
      const data = await knex('tabor_view_table').returning('*');
      return data;
    } catch (error) {
      throw new web.WebError('Table view does not exists!', 400);
    }
  }
}
