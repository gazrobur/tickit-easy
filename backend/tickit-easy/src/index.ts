import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as dotenv from 'dotenv';
import { decodeToken } from './util/jwt';
import * as cors from 'cors';

import { AdvancedController } from 'advanced-controllers';
import * as Controllers from './controllers';

dotenv.config();
const app = express();
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
  cors({
    origin: (origin, callback) => callback(null, true),
    credentials: true
  })
);
app.use(async (req, res, next) => {
  const token = req.cookies['x-nodebus-token']
    ? req.cookies['x-nodebus-token']
    : null;
  if (!token) {
    req.user = undefined;
  } else {
    try {
      req.user = await decodeToken(token);
      req.user.hasPermission = (permission: string) => 
        req.user.permissions.includes(permission);
    } catch (error) {
      console.log('Error while reading the token.');
    }
  }

  next();
});

app.get('/', (request, response) => {
  response.send('Hello World!');
});

app.listen(process.env.PORT || 3000, () =>
  console.log(`App is listening on port ${process.env.PORT || 3000}`)
);

const controllerObjs = [];
for (const key of Object.keys(Controllers)) {
  controllerObjs.push(new Controllers[key]());
}

AdvancedController.registerAll(app, { implicitAccess: true });
