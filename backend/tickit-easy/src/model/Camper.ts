interface ICamper extends ICamperService {
  id: string;
  name: string;
  sex: string;
  country: string;
  state: string;
  city: string;
  email: string;
  phone: string;
  camp: string;

  // Ez egy enum lesz ami AKTIV, TOROLT, INAKTIV
  status: string;
}

interface ICamperService {
  serviceOne: string;
  serviceTwo: string;
}

export { ICamper, ICamperService };
