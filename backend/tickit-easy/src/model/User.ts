interface IBaseUser {
  fullName: string;
  email: string;
  password: string;
}

interface IUser extends IBaseUser {
  id?: string;
  status: boolean;
}

interface ILoginUser {
  email: string;
  password: string;
}

class User {
  id: string;
  fullName: string;
  email: string;
  status: boolean;

  constructor(userObj: IUser) {
    this.id = userObj.id;
    this.fullName = userObj.fullName;
    this.email = userObj.email;
    this.status = userObj.status;
  }
}

export { IBaseUser, IUser, User, ILoginUser };
