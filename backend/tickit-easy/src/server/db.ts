import * as local from '../../knexfile';
// import {knex} from './remoteDb';
import * as knex from 'knex';

/** Choose between databases: own, ifi */
export default (v = 'own') => {
  if (v === 'own') return knex(local);
  // if (v === 'ifi') return knex(remote);
};
