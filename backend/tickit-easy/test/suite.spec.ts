import * as chai from 'chai';
import chaiHttp = require('chai-http');

const baseUrl = 'http://localhost:3000';

chai.use(chaiHttp);

describe('a suite of tests', function() {
  this.timeout(500);

  it('should take less than 500ms', function(done) {
    setTimeout(done, 300);
  });

  it('should take less than 500ms as well', function(done) {
    setTimeout(done, 250);
  });
});
