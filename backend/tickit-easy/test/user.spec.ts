import * as chai from 'chai';
import chaiHttp = require('chai-http');

const baseUrl = 'http://localhost:3000';

chai.use(chaiHttp);

describe('user test', () => {
  it('should start empty', () => {
    const arr = [];
    chai.assert.equal(arr.length, 0);
  });

  it('should give unauthorized', () => {
    chai
      .request(baseUrl)
      .get('/user/all')
      .then((res) => {
        // we cant get there
        console.log("?HOW?? ",res);
      })
      .catch((err) => {
        chai.assert.equal(err.status, 401);
      });
  });

  it('should give 404', () => {
    chai
      .request(baseUrl)
      .get('/asdasdsasd')
      .then((res) => {
        // we cant get there
        console.log("?HOW?? ",res);
      })
      .catch((err) => {
        chai.assert.equal(err.status, 404);
      });
  });
});
