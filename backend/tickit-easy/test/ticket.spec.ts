import * as chai from 'chai';
import chaiHttp = require('chai-http');

const baseUrl = 'http://localhost:3000';

chai.use(chaiHttp);

describe('ticket tests', () => {
  it('should get tickets list', () => {
    chai
      .request(baseUrl)
      .get('/ticket/all')
      .then((res) => {
        chai.assert.equal(res.status, 200);
        chai.assert.equal(res.body.length, 1);
      })
      .catch((err) => {
        console.log(err);
      });
  });

});
