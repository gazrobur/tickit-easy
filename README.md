# Projekt eszközök beadandó feladat
*Készült Erdősi Péter, Iván János Benedek, Magyar Ádám és Solti Péter keze által*
### Tickit-Easy jegyvásárló felület 

**Egy jegyvásárlást lebonyolító applikáció, amelyben egyszerre több tranzakciót lehet kezelni**

Beadandó feladat az ELTE Informatikai Karon, Projekt eszközök nevű tárgyból.

	
### Fejlesztői környezet, használt technológiák

- Visual Studio Code kódszerkesztő
- NodeJS
- Angular (+Angular Material)
- Modern JavaScript ES6


### Futtatható környezet felállítása

Egy (Gitlab) Runner által futtatjuk a .gitlab-ci.yml-ben definiált feladatokat minden commitra:
- build: 
	- backend build: A backend/tickit-easy mappába lépve meghívjuk az npm install metódust argumentum nélkül, amellyel letöltjük a package.json file csomagban definiált függőségeket és legeneráljuk a node_modules mappát a telepített modulokkal együtt, majd buildeljük.
	- frontend build: A frontend/tickit-easy mappába lépve meghívjuk az npm install metódust argumentum nélkül, amellyel letöltjük a package.json file csomagban definiált függőségeket és legeneráljuk a node_modules mappát a telepített modulokkal együtt, majd buildeljük.
- test: 
	- unit tests: A backend/tickit-easy mappába lépve meghívjuk az npm install metódust argumentum nélkül, amellyel letöltjük a package.json file csomagban definiált függőségeket és legeneráljuk a node_modules mappát a telepített modulokkal együtt, majd futtatjuk a "test" nevű scriptünket.
	- e2e tests: A frontend/tickit-easy mappába lépve meghívjuk az npm install metódust argumentum nélkül, amellyel letöltjük a package.json file csomagban definiált függőségeket és legeneráljuk a node_modules mappát a telepített modulokkal együtt, letöltjük a Selenium server.jar-t és a hozzá tartozó chromedriver könyvtárat, ezekután elindítjuk a Seleniumot a háttérben, majd futtatjuk az e2e teszteket.
		

### Extra eszközök

Az általunk választott extra eszközök úgy lettek konfigurálva, hogy azok pushra autómatikusan lefussanak és ha eltörnek, akkor törik a build is utána:
- Typedoc: 
	- Funkció: A TypeScriptünk forráskódjában talált kommentek felhasználásával készít egy HTML dokumentációt vagy egy JSON Modelt.
	- Kimenet: Sikeres futás esetén kiad egy docs mappát a frontednél.
- TSLint:
	- Funkció: Ellenőrzi a TypeScript kódunkat olvashatósági, karbantartási és funkcionalitási hibákra. Lehetőségünk van a saját szabályaink, konfigurációnk általi beállítására.
	- Kimenet: Kiírja, hogy sikeres volt (passed) vagy hiba esetén jelez.
	